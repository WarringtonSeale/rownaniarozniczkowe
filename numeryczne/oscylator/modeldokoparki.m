function [dA] = modeldokoparki(t, y)


%danezpoprz
x1_dot = y(1);
x2_dot = y(2);
x1 = y(3);
x2 = y(4);

%parametry
m1 = 1;
m2 = 1;
k1 = 100;
k2 = 100;
b1 = 1;
b2 = 1;
w = 10;
A = 2;

x0 = A*sin(w*t);
x0_dot = A*cos(w*t);

%macierzmas
Mm = sparse(4, 4);
Mm(1, 1) = m1;
Mm(2, 2) = m2;
Mm(3, 3) = 1;
Mm(4, 4) = 1;

%macierzsztywnosci

Ms = [-k1*x1+k1*x2+b1*x2_dot-b1*x1_dot;
    -k1*x2+k1*x1+k2*x0-k2*x2-b1*x2_dot+b1*x1_dot+b2*x0_dot-b2*x2_dot;
    x1_dot;
    x2_dot];

dA = Mm\Ms;

end
 
