clc;
clear;

A = 2;
w = 10;

tk = 30;
tr = [0, tk];
y0 = [0, 0, 0, 0];

[t, y] = ode45('modeldokoparki', tr, y0);

x0 = A.*sin(w*t);
x0_dot = A.*cos(w*t);

figure(1)
subplot(2, 2, 1);
plot(t, y(:, 1), 'b', t, x0_dot, 'r')
legend('x1_{dot}', 'x0_{dot}')
subplot(2, 2, 2);
plot(t, y(:, 2), 'b', t, x0_dot, 'r')
legend('x2_{dot}', 'x0_{dot}')
subplot(2, 2, 3);
plot(t, y(:, 3), 'b', t, x0, 'r')
legend('x1', 'x0');
subplot(2, 2, 4);
plot(t, y(:, 4), 'b', t, x0, 'r')
legend('x2', 'x0'); 
