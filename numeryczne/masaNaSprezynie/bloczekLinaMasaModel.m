function [dA] = bloczekLinaMasaModel(t, y)
%zmienne
f_dot = y(1);
x_dot = y(2);
f = y(3);
x = y(4);

%stale
m1 = 1;
m2 = 1;
k = 100;
r = 1;
g = 9.81;

% sila/moment rownowazacy
F = m1*g;
M = m1*g*r;
%mac mas
Mm=sparse(4, 4);
Mm(1, 1) = 0.5*m2*r*r;
Mm(2, 2) = m1;
Mm(3, 3) = 1;
Mm(4, 4) = 1;

%mac sztywn
Ms = [-k*f*r*r+k*x*r;
      m1*g-k*x+k*f*r;
      f_dot;
      x_dot];
%sila-moment
rownowazenieSila = false;        %Zalaczanie / wylaczanie rownowazenia sila przylozona do masy
rownowazenieMomentem = true;      %Zalaczanie / wylaczanie rownowazenia momentem przyozonym do bloczka

if rownowazenieSila == true
    Ms(2, 1) = m1*g-k*x+k*f*r-F;
end

if rownowazenieMomentem == true
    Ms(1, 1) = -k*f*r*r+k*x*r-M;
end

dA = Mm\Ms;


end 
