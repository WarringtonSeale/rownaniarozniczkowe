clc;
clear all;

tk = 20;
tr = [0, tk];

y0 = [0; 0; 0; 0];


[t, y] = ode45('bloczekLinaMasaModel', tr, y0);

figure(1);
subplot(2, 2, 1);
plot(t, y(:, 1));
title('$\dot{\phi}$','interpreter','latex', 'fontsize', 20);
subplot(2, 2, 2);
plot(t, y(:, 2));
title('$\dot{x}$','interpreter','latex', 'fontsize', 20);
subplot(2, 2, 3);
plot(t, y(:, 3));
title('$\phi$','interpreter','latex', 'fontsize', 20);
subplot(2, 2, 4);
plot(t, y(:, 4));
title('$x$','interpreter','latex', 'fontsize', 20); 
