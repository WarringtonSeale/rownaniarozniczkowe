clc;
clear all;

r = 4.7;
l = 220*1e-6;
c = 470*1e-9;
a=1;

tk = 0.01;
tr = [0, tk];

y0=[0; 0];

[t, y] = ode45(@(t, y) rlcmodel(t, y, r, l, c, a), tr, y0);

figure(1);
subplot(2, 1, 1);
plot(t, y(:,1));
title('prad');

subplot(2, 1, 2);
plot(t, y(:,2)*c);
title('ladunek');
 
