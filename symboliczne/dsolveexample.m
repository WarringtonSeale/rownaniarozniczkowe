clc; clear;

% mamy rownianie  mx'' + bx' + kx = sin(5t)
% i war. poczatkowe dx(0) = 0, d2x(0)=0

syms x(t)
% ^ definiujemy rownanie jako funkcje symboliczna

m = 7;
b = 5;
k = 112;
%^ jak mamy jakies stale w rownaniu to warto je tutaj zdefiniowac


dx = diff(x, t);
d2x = diff(x, t, 2);
%^ dla uproszczenia zapisu warto zdefiniowac dla siebie ''skroty''
%pochodnych

rownanie = d2x == -(b/m)*dx - (k/m)*x + sin(5*t);
%^ zapisujemy rozwiazywane rownanie - warto zwrocic uwage na to, w miejscu
% gdzie w naszym rownaniu jest znak rowna sie zostaje on zastapiony
% podwojnym znakiem rownosci; - pierwszy znak przypisuje rownanie pod
% zmienna rownanie

warunki = [dx(0) == 12, d2x(0)==0];
%^ podobnie jak w poprzednim przypadku jest podwojny znak rownosci -
%kolejne warunki poczatkowe zapisujemy w wektorze

rozwiazanie = dsolve(rownanie, warunki)
%^ rozwiazujemy - jako rozwiazanie otrzymamy funkcje symboliczna

pretty(rozwiazanie)
%^ ladnie drukujemy rozwiazanie w konsoli

figure(1)
fplot(rozwiazanie)
%^ przyklad wykresu funkcji symbolicznej 


%Jezeli potrzebne sa wartosci numeryczne obliczonej funkcji to tworzymy
%interesujacy nas wektor czasu i wyznaczamy wartosci

wektorczasu = linspace(0, 15, 100) 
%^ wektor od 0 do 15 z 10000 wartosci rownomiernie rozmieszczonych


wychylenie = double(subs(rozwiazanie, t, wektorczasu))
%^ oblicz double(costam) konwertuje dane z wyrazenia symbolicznego do wartosci numerycznej
% subs jest podstawieniem pod zmienna; w rozwiazaniu naszego rownania za t
% podstawiamy po kolei wartosci z wektora czasu

figure(2)
plot(wektorczasu, wychylenie)
%^ wszystkie dane sa juz w postaci numerycznej - mozna po ludzku strzelic
%wykresik plotem
